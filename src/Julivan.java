package robo;
import robocode.*;
import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * Van - a robot by  Julivan Soares
 */
public class Julivan extends AdvancedRobot
{
  int campo=90;
  int campo2=80;
  int numdi=90; 
  int numesq=80;
	public void run() {
		
   
     setBodyColor(Color.black);
     setGunColor(Color.black);
     setRadarColor(Color.black);
     setScanColor(Color.black);
     setBulletColor(Color.black);
	
	
while(true) {



for(int voltadi=0; voltadi<5; voltadi++){
	setAhead(campo);
	setTurnRight(numdi);
	campo=campo+5;
    numdi=numdi+5;
	execute();
}

for(int voltaesq=5; voltaesq>0; voltaesq--){
	setAhead(campo2);
	setTurnGunLeft(numesq);
    campo2=campo2+5;
    numesq=numesq+5;
	execute();
}



}//	while


}//run

	
   

	public void onScannedRobot(ScannedRobotEvent e) {
	       
       	fire(2);
		
     } 


	/**
	 * 
     se levar tiro  
      
	 */ 
	public void onHitByBullet(HitByBulletEvent e) {  
	
   
           fire(2);		
         setAhead(200);
	}
	

	public void onHitWall(HitWallEvent e) {
	  setTurnGunLeft(80);
	   setAhead(100);

           
	}	
}
