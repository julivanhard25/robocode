# Robocode


   # Robocode
 
 ## Descrição 
 
 Este robô foi criado para o **desafio solutis robot** arena, utilizando a tecnologia robocode.
 
 
   ## Tecnologia utilizadas 
   
- Linguagem Java
- Robocode 1.9.3.9 
 
 ## Funcionamento do robô<br>
 O codigo possui um laço de repetição infinito, onde são executados dois laços de repetição que são responsaves por: movimentar o robô para frente e gira-lo para direita e em seguida as variáves de movimentação do robô e direção recebem um incremento de mais 5, assim o robô sempre vai estar se movimentando na arena.
 
``` java
for(int voltadi=0; voltadi<5; voltadi++){
	setAhead(campo);
	setTurnRight(numdi);
	campo=campo+5;
    numdi=numdi+5;
	execute();
}
```
 Movimnte e girar o canhão para a esquerda e em seguida as variáves deentar o robô para fre movimentação do robô e direção do giro do canhão recebem um incremento de mais 5, assim o robô sempre vai estar sempre movimentando o canhão enquanto anda na arena.
 
``` java 
for(int voltaesq=5; voltaesq>0; voltaesq--){
	setAhead(campo2);
	setTurnGunLeft(numesq);
    campo2=campo2+5;
    numesq=numesq+5;
	execute();
}
```

## Pontos fortes<br>
 Foi realizado uma alteração na classe do robô que antes era `robocode` para `advancedrobot` assim os movimentos do mesmo ficaram melhores, já que  agora os comandos são executados de forma simutanêa, dando um  melhor desempenho para  o robô. 





## Pontos fracos<br>
a potencia do tiro que foi utilizada foi a de numero 2 e também quando o robô for acertado o tiro de resposta sera de potencia 2 para que o dano no oponente seja maior Por causa da alteração do tipo de tiro para um mais potente o tempo de reação cai um pouco se comparado o tiro de potencia 1 e isso pode ser uma desvantagem.
<br>
<br>




